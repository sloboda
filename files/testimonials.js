var fcontent=new Array();
begintag=''; //set opening tag, such as font declarations

fcontent[0] = "&quot;The internet has provided easy access to a lot of commerce in our society. Most of it is good, a lot is average and some is very bad. Yours is excellent.&quot; <em>- Cefalu Family Vineyards</em>";
fcontent[1] = "&quot;Just had to tell you that one of our customers called from California and said our website is shoulders above any other GPS website out there! Thank you!!&quot; <em>- Atomic GPS</em>";
fcontent[2] = "&quot;You did an outstanding job for us and it is greatly appreciated. Above and beyond what I would have expected when going into this.&quot; <em>- IM2 Consulting</em>";
fcontent[3] = "&quot;It has been a real pleasure to work with people who are so responsive, prompt and creative.&quot; <em>- Len Consult</em>";
fcontent[4] = "&quot;I would also like to thank you for your tireless effort, professionalism, creativity and above all for the timely response you have been giving us.&quot; <em>- Adtech</em>";
fcontent[5] = "&quot;Just awesome: You are the best. You can use me as a reference anytime!&quot; <em>- Discover Danville Association</em>";
fcontent[6] = "&quot;This was the first long-term project we've worked out without getting upset with the vendor. You really impressed me personally, and as a company.&quot; <em>- Davies Computer Solutions</em>";
fcontent[7] = "&quot;WOW! You have out-done yourself! Thanks so much.... I may have so much business, you'll have to come help me cook!&quot; <em>- Brass Lantern Manor</em>";
fcontent[8] = "&quot;The website looks great! With the greatest respect for the excellent work that you and your team do, thank you.&quot; <em>- Allessandrea World Enterprises</em>";
fcontent[9] = "&quot;All the work was extremely professional and completed within a timely manner.&quot; <em>- Bright Future Learning Center</em>";


closetag='';
/***********************************************
* Fading Scroller- � Dynamic Drive DHTML code library (www.dynamicdrive.com) * This notice MUST stay intact for legal use * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

var delay = 8000; //set delay between message change (in miliseconds)
var maxsteps=30; // number of steps to take to change from start color to endcolor
var stepdelay=40; // time in miliseconds of a single step
//**Note: maxsteps*stepdelay will be total time in miliseconds of fading effect
var startcolor= new Array(40,177,218); // start color (red, green, blue)
var endcolor=new Array(0,118,154); // end color (red, green, blue)



var fwidth='500px'; //set scroller width
var fheight='60px'; //set scroller height

var fadelinks=1;  //should links inside scroller content also fade like text? 0 for no, 1 for yes.

///No need to edit below this line/////////////////


var ie4=document.all&&!document.getElementById;
var DOM2=document.getElementById;
var faderdelay=0;
var index=0;


/*Rafael Raposo edited function*/
//function to change content
function changecontent(){
  if (index>=fcontent.length)
    index=0
  if (DOM2){
    document.getElementById("fscroller").style.color="rgb("+startcolor[0]+", "+startcolor[1]+", "+startcolor[2]+")"
    document.getElementById("fscroller").innerHTML=begintag+fcontent[index]+closetag
    if (fadelinks)
      linkcolorchange(1);
    colorfade(1, 15);
  }
  else if (ie4)
    document.all.fscroller.innerHTML=begintag+fcontent[index]+closetag;
  index++
}

// colorfade() partially by Marcio Galli for Netscape Communications.  ////////////
// Modified by Dynamicdrive.com

function linkcolorchange(step){
  var obj=document.getElementById("fscroller").getElementsByTagName("A");
  if (obj.length>0){
    for (i=0;i<obj.length;i++)
      obj[i].style.color=getstepcolor(step);
  }
}

/*Rafael Raposo edited function*/
var fadecounter;
function colorfade(step) {
  if(step<=maxsteps) {	
    document.getElementById("fscroller").style.color=getstepcolor(step);
    if (fadelinks)
      linkcolorchange(step);
    step++;
    fadecounter=setTimeout("colorfade("+step+")",stepdelay);
  }else{
    clearTimeout(fadecounter);
    document.getElementById("fscroller").style.color="rgb("+endcolor[0]+", "+endcolor[1]+", "+endcolor[2]+")";
    setTimeout("changecontent()", delay);
	
  }   
}

/*Rafael Raposo's new function*/
function getstepcolor(step) {
  var diff
  var newcolor=new Array(3);
  for(var i=0;i<3;i++) {
    diff = (startcolor[i]-endcolor[i]);
    if(diff > 0) {
      newcolor[i] = startcolor[i]-(Math.round((diff/maxsteps))*step);
    } else {
      newcolor[i] = startcolor[i]+(Math.round((Math.abs(diff)/maxsteps))*step);
    }
  }
  return ("rgb(" + newcolor[0] + ", " + newcolor[1] + ", " + newcolor[2] + ")");
}

if (ie4||DOM2)
  document.write('<div id="fscroller" style="padding:0 0 0 10px;height:'+fheight+';width:'+fwidth+';"></div>');

if (window.addEventListener)
window.addEventListener("load", changecontent, false)
else if (window.attachEvent)
window.attachEvent("onload", changecontent)
else if (document.getElementById)
window.onLoad=changecontent
document.write('<p align="right"><a href="client-testimonials.php">&gt; More kind words from our clients</a></p>');
